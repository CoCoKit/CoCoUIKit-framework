//
//  CoCoSinglePopUpTrackingSlider.h
//  CoCoSinglePopUpTrackingSlider
//
//  Created by 陈明 on 2018/4/17.
//  Copyright © 2018年 新浪微博. All rights reserved.
//

/**
 * 可以使用代码加载或xib


   1.颜色slider
   CoCoSinglePopUpTrackingSliderConfig *config = [[CoCoSinglePopUpTrackingSliderConfig alloc] init];
   config.indicateViewOffset = 3;
   config.blockSize = CGSizeMake(28, 28);
   config.blockCornerRadius = 14;
   config.indicateViewWidth = 50;
   config.indicateViewHeight = 56;
   config.indicateViewArrowHeight = 6;
   config.indicateViewCornerRadius = 25;
   config.popViewType = CoCoSinglePopUpTypeColor;
   config.showTimeType = CoCoSinglePopUpShowTimeTouched;
   config.progressHeight = 6;
   config.progressRadius = 3;
   CoCoSinglePopUpTrackingSlider *slider1 = [[CoCoSinglePopUpTrackingSlider alloc] initWithFrame:CGRectMake(10, 360, [UIScreen mainScreen].bounds.size.width-20, 70) config:config];

   slider1.sliderValueChanged = ^(CGFloat minValue) {
   NSLog(@"%f",minValue);
   } ;

   slider1.sliderColorChanged = ^(UIColor *color) {
   NSLog(@"%@",color);
   };

   [self.view addSubview:slider1];


   2.xib方式
   self.testSlider.config.blockSize = CGSizeMake(20, 20);
   self.testSlider.config.indicateViewOffset = 10;
   [self.testSlider applyConfig];


 */



#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    CoCoSinglePopUpTypeValue,
    CoCoSinglePopUpTypeColor,
} CoCoSinglePopUpType;

typedef enum : NSUInteger {
    CoCoSinglePopUpShowTimeTouched,// 触摸才显示
    CoCoSinglePopUpShowTimeAlways,// 总是显示
} CoCoSinglePopUpShowTime;

typedef enum : NSUInteger {
    CoCoTrackingSliderPopUpIndicateDirectionNomal,
    CoCoTrackingSliderPopUpIndicateDirectionLeft,
    CoCoTrackingSliderPopUpIndicateDirectionRight,
} CoCoTrackingSliderPopUpIndicateDirection;

@interface CoCoSinglePopUpTrackingSliderConfig : NSObject

@property (nonatomic, assign) CoCoSinglePopUpType popViewType; // 默认value
@property (nonatomic, assign) CoCoSinglePopUpShowTime showTimeType;// 默认always
@property (nonatomic, assign) CGFloat minValue;
@property (nonatomic, assign) CGFloat maxValue;
@property (nonatomic, assign) CGFloat defaultValue;// 默认0
@property (nonatomic, strong) UIColor *defaultColor;

@property (nonatomic, assign) CGFloat progressHeight;// 横条高度 默认 10
@property (nonatomic, assign) CGFloat progressRadius;// 横条圆角 默认 5
@property (nonatomic, assign) CGFloat progressLeftSpace;// 左边距 默认 20
@property (nonatomic, assign) CGFloat progressRightSpace;// 右边距 默认 20
@property (nonatomic, strong) UIColor *blockLeftColor;// 滑块左侧color 默认f2f2f2
@property (nonatomic, strong) UIColor *blockRightColor;// 滑块右侧color 默认f2f2f2

@property (nonatomic, assign) CGFloat indicateViewOffset;// 滑块和弹出的指示视图之间的间距 默认 3
@property (nonatomic, assign) CGFloat indicateViewWidth;// 指示视图宽度 默认 30
@property (nonatomic, assign) CGFloat indicateViewHeight;// 指示视图高度 默认 30
@property (nonatomic, assign) CGFloat indicateViewCornerRadius;// 指示视图圆角半径 默认 5
@property (nonatomic, assign) CGFloat indicateViewArrowWidth;// 箭头宽度 默认 6
@property (nonatomic, assign) CGFloat indicateViewArrowHeight;// 箭头高度 默认4
@property (nonatomic, assign) CGFloat indicateViewArrowOffset;// 箭头上移或下移 默认0  （应对圆形可能出现空隙）
@property (strong, nonatomic) UIColor *backIndicateColor;// 指示视图默认背景色 默认 d0d0d0
@property (nonatomic, assign) CGFloat indicateTextSize;// 指示器默认文字大小 默认14
@property (nonatomic, strong) UIColor *indicateTextColor;// 指示视图默认文字颜色 默认 白色

@property (nonatomic, assign) CGSize blockSize;// 默认 {35,35}
@property (nonatomic, assign) CGFloat blockCornerRadius;// 默认 35/2.f
@property (nonatomic, strong) UIImage *blockImage;// 可选，如果有image，则不使用blockColor，也不使用maskToBounds
@property (nonatomic, strong) UIColor *blockColor;// 默认 白色
@property (nonatomic, assign) CGFloat blockBottomSpace;// 滑块底部与主视图的距离 默认2


@property (nonatomic, assign) BOOL showIndicateView;// 是否显示指示视图,默认为YES
@end

@interface CoCoTrackingSliderPopUpView : UIView
/**
   背景色
 */
@property (strong, nonatomic) UIColor *backIndicateColor;
@property (strong, nonatomic, readonly) UILabel *indicateLabel;
@property (nonatomic, assign) CGFloat indicateViewArrowWidth;// 箭头宽度
@property (nonatomic, assign) CGFloat indicateViewArrowHeight;// 箭头高度



- (instancetype)initWithConfig:(CoCoSinglePopUpTrackingSliderConfig *)config;

/**
   设置标题

   @param title 设置标题
 */
- (void)setTitle:(NSString *)title;

/**
   设置指示方向

   @param direction 设置指示方向
 */
- (void)setDirection:(CoCoTrackingSliderPopUpIndicateDirection)direction;

/**
   设置指示方向为正常状态
 */
- (void)setDirectionAnimateToNomal;
@end


@interface CoCoSinglePopUpTrackingSlider : UIControl

@property (nonatomic, strong, readonly) CoCoSinglePopUpTrackingSliderConfig *config;

@property (assign, nonatomic) CGFloat currentValue;// 当前小滑块所代表的值，可设置
@property (nonatomic, strong) UIColor *currentColor;// 当前颜色

- (instancetype)init __attribute__((unavailable("禁止使用init方法，请使用头文件中其他初始化方法")));
+ (instancetype)new __attribute__((unavailable("禁止使用new方法，请使用头文件中其他初始化方法")));

- (instancetype)initWithFrame:(CGRect)frame config:(CoCoSinglePopUpTrackingSliderConfig *)config;
- (void)applyConfig;

@property (copy, nonatomic) void (^ sliderValueChanged)(CGFloat minValue);
@property (copy, nonatomic) void (^ sliderColorChanged)(UIColor *color);
/**
   获取小指标的标题
 */
@property (copy, nonatomic) NSString * (^ getMinTitle)(CGFloat minValue);


@end
