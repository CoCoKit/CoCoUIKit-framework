//
//  CoCoButton.h
//  CoCoUIKit
//
//  Created by 陈明 on 2018/1/17.
//  Copyright © 2018年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    CoCoButtonIconLayoutLeft,
    CoCoButtonIconLayoutRight,
    CoCoButtonIconLayoutBottom,
    CoCoButtonIconLayoutTop,
} CoCoButtonIconLayout;

@interface CoCoButton : UIControl
@property (nonatomic, strong) UIImage *normalImage;
@property (nonatomic, strong) UIImage *highlightImage;
@property (nonatomic, strong) UIImage *disableImage;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) CoCoButtonIconLayout iconLayout;
@property (nonatomic, assign) UIEdgeInsets iconEdgeInsets;
@end
