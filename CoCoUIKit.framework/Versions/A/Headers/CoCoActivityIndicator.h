//
//  CoCoActivityIndicator.h
//  CoCoUIKit
//
//  Created by 陈明 on 2017/5/12.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoCoActivityIndicator : UIView
/**
 *  Total number of items that will take part in activity animation.
 *  Default to 6.
 */
@property (nonatomic) int32_t maxItems;

/**
 *  Radius of invisible circle around which items are moved.
 */
@property (nonatomic) int32_t radius;

/**
 *  Total duration of full cycle from 0..2PI that items make turning around the circle.
 */
@property (nonatomic) NSTimeInterval cycleDuration;

/**
 *  Defines min size for item.
 */
@property (nonatomic) CGSize minItemSize;

/**
 *  Defines max size for item. First item will always have max size.
 *  Other item sizes are interpolated between max..min size.
 */
@property (nonatomic) CGSize maxItemSize;

/**
 *  Defines max speed coefficient that item may have.
 *  First item will get max speed (if there are more than one item).
 *  Other items speed will be interpolated between maxSpeed and 1.0.
 *  Last item will always have speed of 1.0.
 *  Default to 1.6.
 */
@property (nonatomic) CGFloat maxSpeed;

/**
 *  First control point of cubic bezier curve.
 */
@property (nonatomic) CGPoint firstBezierControlPoint;

/**
 *  Second control point of cubic bezier curve.
 */
@property (nonatomic) CGPoint secondBezierControlPoint;

/**
 *  Property acts like in UIActivityIndicatorView.
 *  When indicator isn't animating - it will be hidden if property set to YES.
 *  Default to NO.
 */
@property (nonatomic) BOOL hidesWhenStopped;

/**
 *  Custom image for item. Preffered size for it is maxItemSize.
 *  If you don't want to provide image - set itemColor property.
 *  Default to nil.
 */
@property (nonatomic, strong) UIImage *itemImage;

/**
 *  Item color in case if item image isn't provided.
 *  itemImage gets priority if both of properties are set.
 *  Default to white.
 */
@property (nonatomic, strong) UIColor *itemColor;

/**
 *  Tells if activity indicator currently animating.
 */
@property (nonatomic, readonly) BOOL isAnimating;

/**
 *  Begins fancy activity indicator animation using cubic Bezier curve as interpolation function.
 */
- (void)startAnimating;

/**
 *  Stops current activity indicator animation.
 */
- (void)stopAnimating;
@end
