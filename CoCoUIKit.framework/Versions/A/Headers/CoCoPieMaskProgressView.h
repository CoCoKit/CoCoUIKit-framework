//
//  CoCoPieMaskProgressView.h
//  CoCoUIKit
//
//  Created by 陈明 on 2017/6/17.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIView+CoCoPieMaskProgressView.h"


/*!
   @abstract 覆盖的颜色
 */
#define kCoCoPieMaskProgressViewBackgroundColor [UIColor colorWithWhite:0 alpha:0.3]

// 半径是宽的多少比例
static CGFloat const kCoCoPieMaskProgressViewRidusRate = 0.55;

// 空白的大小是宽度的多少比例
static CGFloat const kCoCoPieMaskProgressViewRingWidth = 1;
static CGFloat const kCoCoPieMaskProgressViewRingMargin = 2;

@interface CoCoPieMaskProgressView : UIView

/*!
   @abstract 进度，0到1
 */
@property (nonatomic) CGFloat progress;

@end
