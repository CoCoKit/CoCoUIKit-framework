//
//  DGActivityIndicatorView.h
//  DGActivityIndicatorExample
//
//  Created by Danil Gontovnik on 5/23/15.
//  Copyright (c) 2015 Danil Gontovnik. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    CoCoActivityHUDStyleDoubleBounce = 1,
    CoCoActivityHUDStyleFiveDots = 2,
    CoCoActivityHUDStyleNineDots = 3,
    CoCoActivityHUDStyleRotatingSquares = 4,
    CoCoActivityHUDStyleTriplePulse = 5,
    CoCoActivityHUDStyleTwoDots = 6,
} CoCoActivityHUDStyle;


@interface DGActivityIndicatorView : UIView

- (id)initWithType:(CoCoActivityHUDStyle)type;
- (id)initWithType:(CoCoActivityHUDStyle)type tintColor:(UIColor *)tintColor;
- (id)initWithType:(CoCoActivityHUDStyle)type tintColor:(UIColor *)tintColor size:(CGFloat)size;

@property (nonatomic) CoCoActivityHUDStyle type;
@property (nonatomic, strong) UIColor *tintColor;
@property (nonatomic) CGFloat size;

@property (nonatomic, readonly) BOOL animating;

- (void)startAnimating;
- (void)stopAnimating;

@end
