//
//  UIView+CoCoPopupView.h
//  CoCoUIKit
//
//  Created by 陈明 on 2017/10/27.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoCoPopupView.h"

@interface UIView (CoCoPopupView)

- (void)showPopView:(CoCoPopupView *)popView AtPoint:(CGPoint)point;
- (void)hidePopView;

@end
