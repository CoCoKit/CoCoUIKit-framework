//
//  CoCoSwitch.h
//  Interest inducing
//
//  Created by 陈明 on 18/4/13.
//  Copyright (c) 2018年 . All rights reserved.
//

/**
 * 可以使用xib加载，也可以纯代码加载

   1.纯代码加载:

   CoCoSwitchConfig *config = [[CoCoSwitchConfig alloc] init];
   config.onText = @"完成";
   config.offText = @"完成";
   config.defaultOn = NO;
   config.onTextXOffset = -2;
   config.offTextXOffset = 2;
   CoCoSwitch *swit = [[CoCoSwitch alloc] initWithFrame:CGRectMake(60, 150, 52, 20) config:config];

   [self.view addSubview:swit];
   [swit addTarget:self action:@selector(switchSex:) forControlEvents:UIControlEventTouchUpInside];



   2.xib加载后

   @property (weak, nonatomic) IBOutlet CoCoSwitch *switcher;
   self.switcher.config.switchKnobSize = 38;
   [self.switcher applyConfig];   //更新完config要使用applyConfig来更新到界面

 */

#import <UIKit/UIKit.h>


@interface CoCoSwitchConfig : NSObject

@property (nonatomic, strong) UIColor *onBackgroundColor;
@property (nonatomic, strong) UIColor *offBackgroundColor;

@property (nonatomic, strong) UIColor *onBallColor;
@property (nonatomic, strong) UIColor *offBallColor;

@property (nonatomic, strong) UIColor *onTextColor;
@property (nonatomic, strong) UIColor *offTextColor;

@property (nonatomic, strong) NSString *onText;
@property (nonatomic, strong) NSString *offText;

@property (nonatomic, assign) NSInteger switchKnobSize;
@property (nonatomic, assign) CGFloat fontSize;

@property (nonatomic, assign) CGFloat onTextXOffset;
@property (nonatomic, assign) CGFloat offTextXOffset;

@property (nonatomic, assign) BOOL defaultOn;

@end

@interface CoCoSwitch : UIControl
@property (nonatomic, assign, getter = isOn) BOOL on;
@property (nonatomic, strong, readonly) CoCoSwitchConfig *config;



- (void)setOn:(BOOL)on animated:(BOOL)animated;
- (id)initWithFrame:(CGRect)frame config:(CoCoSwitchConfig *)config;
- (void)applyConfig;

@end
