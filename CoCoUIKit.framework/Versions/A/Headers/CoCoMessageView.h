//
//  CoCoMessageView.h
//  CoCoUIKit
//
//  Created by 陈明 on 2017/9/10.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CoCoMessageView : UIView


/**
 * Loading
 */
+ (void)showLoadingInWindow:(UIWindow *)window;
+ (void)showLoadingInWindow:(UIWindow *)window shouldMask:(BOOL)mask;
+ (void)showLoadingWithMsg:(NSString *)msg inWindow:(UIWindow *)window;
+ (void)showLoadingWithMsg:(NSString *)msg inWindow:(UIWindow *)window shouldMask:(BOOL)mask;

/**
 * Successed
 */
+ (void)showSuccessedInWindow:(UIWindow *)window;
+ (void)showSuccessedInWindow:(UIWindow *)window shouldMask:(BOOL)mask;
+ (void)showSuccessedWithMsg:(NSString *)msg inWindow:(UIWindow *)window;
+ (void)showSuccessedWithMsg:(NSString *)msg inWindow:(UIWindow *)window shouldMask:(BOOL)mask;

/**
 * Error
 */
+ (void)showErrorInWindow:(UIWindow *)window;
+ (void)showErrorInWindow:(UIWindow *)window shouldMask:(BOOL)mask;
+ (void)showErrorWithMsg:(NSString *)msg inWindow:(UIWindow *)window;
+ (void)showErrorWithMsg:(NSString *)msg inWindow:(UIWindow *)window shouldMask:(BOOL)mask;

/**
 * Info
 */
+ (void)showInfo:(NSString *)msg inWindow:(UIWindow *)window;
+ (void)showInfo:(NSString *)msg inWindow:(UIWindow *)window shouldMask:(BOOL)mask;

/**
 * Image
 */
+ (void)showImage:(UIImage *)image inWindow:(UIWindow *)window;
+ (void)showImage:(UIImage *)image inWindow:(UIWindow *)window shouldMask:(BOOL)mask;
+ (void)showImage:(UIImage *)image msg:(NSString *)msg inWindow:(UIWindow *)window;
+ (void)showImage:(UIImage *)image msg:(NSString *)msg inWindow:(UIWindow *)window shouldMask:(BOOL)mask;

/**
 * Update Progress
 */

+ (void)showProgressInWindow:(UIWindow *)window;

+ (void)showProgressInWindow:(UIWindow *)window msg:(NSString *)msg;

+ (void)showProgressInWindow:(UIWindow *)window msg:(NSString *)msg shouldMask:(BOOL)mask;

+ (void)updateProgressInWindow:(UIWindow *)window msg:(NSString *)msg progress:(CGFloat)progress;


+ (void)dismissView:(UIWindow *)window complete:(void (^)(void))completion;
+ (void)dismissView:(UIWindow *)window afterTime:(NSTimeInterval)time complete:(void (^)(void))completion;
@end
