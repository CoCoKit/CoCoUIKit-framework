//
//  UIView+CoCoPieMaskProgressView.h
//  CoCoUIKit
//
//  Created by 陈明 on 2017/6/17.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CoCoPieMaskProgressView)
/*!
   @abstract 在视图中添加进度视图
 */
- (void)addProgressView;

/*!
   @abstract 进度视图的进度，0到1
 */
@property (nonatomic) CGFloat progress;

/*!
   @abstract 移除进度视图
   @param animated 移除时是否需要动画
 */
- (void)removeProgressViewAnimated:(BOOL)animated;
@end
